package com.seop.storemanager.service;

import com.seop.storemanager.entity.Consumer;
import com.seop.storemanager.model.AgeGroupItem;
import com.seop.storemanager.repository.ConsumerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsumerService {
    private final ConsumerRepository consumerRepository;

    public void setConsumerData(String gender, String product, Integer ageGroup, Integer price) {
        Consumer addData = new Consumer();
        addData.setGender(gender);
        addData.setProduct(product);
        addData.setAgeGroup(ageGroup);
        addData.setPrice(price);

        consumerRepository.save(addData);
    }

    public List<AgeGroupItem> getAgeGroupInfo() {
        List<AgeGroupItem> result = new LinkedList<>();
        List<Consumer> originData = consumerRepository.findAll();
        for (Consumer item : originData) {
            AgeGroupItem addItems = new AgeGroupItem();
            addItems.setProduct(item.getProduct());
            addItems.setGender(item.getGender());
            addItems.setAgeGroup(item.getAgeGroup());
            result.add(addItems);
        }
        return result;
    }
}
