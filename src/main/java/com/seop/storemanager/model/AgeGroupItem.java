package com.seop.storemanager.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AgeGroupItem {
    private Integer ageGroup;
    private String gender;
    private String product;

}
