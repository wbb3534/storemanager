package com.seop.storemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ConsumerRequest {
    @NotNull
    @Length(min = 1, max = 5)
    private String gender;
    @NotNull
    @Length(min = 1, max = 20)
    private String product;
    @NotNull
    private Integer ageGroup;
    @NotNull
    private Integer price;
}
