package com.seop.storemanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Consumer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 5)
    private String gender;
    @Column(nullable = false, length = 20)
    private String product;
    @Column(nullable = false, length = 2)
    private Integer ageGroup;
    @Column(nullable = false, length = 10)
    private Integer price;

}
