package com.seop.storemanager.controller;

import com.seop.storemanager.model.AgeGroupItem;
import com.seop.storemanager.model.ConsumerRequest;
import com.seop.storemanager.service.ConsumerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/consumer")
@RequiredArgsConstructor
public class ConsumerController {
    private final ConsumerService consumerService;

    @PostMapping("/data")
    public String setData(@RequestBody @Valid ConsumerRequest request) {
        consumerService.setConsumerData(request.getGender(), request.getProduct(), request.getAgeGroup(), request.getPrice());
        return "등록완";
    }

    @GetMapping("/age-group-info")
    public List<AgeGroupItem> getAgeGroupInfo() {
        List<AgeGroupItem> result = consumerService.getAgeGroupInfo();
        return result;
    }
}
